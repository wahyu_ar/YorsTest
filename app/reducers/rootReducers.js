import {combineReducers} from 'redux';
import splash from './splash';

const rootReducers = combineReducers({
    splash
});


export default rootReducers;